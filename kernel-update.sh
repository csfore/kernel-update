#!/usr/bin/env sh

if [ "$EUID" -ne 0 ]; then
   echo "Run as root."
   exit
fi

PROC=$(( $(nproc) + 1 ))
echo "===BUILDING WITH $PROC PROCESSES==="

cd /usr/src/linux

echo "===MAKING KERNEL==="
make -j$PROC
echo "===INSTALLING MODULES==="
make modules_install
echo "===INSTALLING KERNEL==="
make install

echo "===MAKING GRUB CONFIG==="
grub-mkconfig -o /boot/grub/grub.cfg

echo "===EXITED WITH CODE $?==="
